import React from "react";
import PropTypes from "prop-types";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      numberArray:[],
      errorMessage:''
    };
  }

  handleChange = (event) => {
    var daylist = ["S", "M", "T", "W", "T", "F", "S"];
    var today = new Date();
    var day = today.getDay();
    var tempArray =[];
    var errorMsg;
    var inputLimit = event.target.value;
    if (inputLimit !== ''){
    var arraylimit =parseInt(inputLimit);
    if (!isNaN(arraylimit)) {
      if (arraylimit>0&&arraylimit<1001){
         for (let i = 1; i <= arraylimit;i++){
            let output=i;
              if (!(i % 3) && !(i % 5)) {
              output = daylist[day] + 'izz' + daylist[day]+'uzz';
             }
             else if (!(i % 3)) {
               output = <p style={{ color: 'blue' }}>{daylist[day]}izz</p>;
              }
              else if (!(i % 5)) {
               output = <p style={{ color: 'green' }}>{daylist[day]}uzz</p>;
          }
      tempArray.push(output);
    }
  }
  else{errorMsg = "number is should be between 1-1000"; }}
  else{errorMsg = "input is not a integer";}}
  else{errorMsg = "please enter some integer";}
    this.setState({ numberArray: [],errorMessage:errorMsg},function(){
       this.updateArrayLimit(tempArray);
    });
  }
  updateArrayLimit(tempArray){
    this.setState({ numberArray:Object.assign(this.state.numberArray,tempArray) });
  }
  populatingArray(){

  }
  render() {
    return (
      <>
        <h1>React Fizz-Buzz App</h1>
        <input
          type="text"
          name="limit"
          onChange={this.handleChange}/>
        <ul>
          {this.state.numberArray.map(item => {
            return <li>{item}</li>;
          })}
        </ul>
        <h4 style={{color:'red'}}>{this.state.errorMessage}</h4>
      </>
    );
  }
}
App.propTypes = {
  arraylimit: PropTypes.number
};
export default App;